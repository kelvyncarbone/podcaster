## About Podcaster
Podcaster is a package developed by Kelvyn Carbone to extract audio from video with some additional methods like:

- Transconding
- Auto subtitles
- Save at AWS S3 Bucket

## Requirements

- FFMPEG

## HOW TO INSTALL / CONFIG & REQUIREMENTS
NOTE: Your server needs FFMPEG installed! 

In your composer.json in section repositories add this item:

    "repositories": [
        {
            "type": "git",
            "url": "https://gitlab.com/kelvyncarbone/podcaster"
        }
    ],

After that run this command:
        
    composer require kelvyncarbone/podcaster

We have some packages that are required to podcaster works fine:

- "league/flysystem-aws-s3-v3": "^1.0",
- "php-ffmpeg/php-ffmpeg": "^0.14.0",
- "torann/podcastfeed": "^0.2.2"

Add line in app.php -> providers:

    \KelvynCarbone\Podcaster\PodcasterServiceProvider::class
        
Configure your FFMPEG paths in your .env file, like this example:

    FFMPEG_PATH="C:\ffmpeg\bin\ffmpeg.exe"
    FFPROBE_PATH="C:\ffmpeg\bin\ffprobe.exe"

Run command to publish the files:
        
    php artisan vendor:publish --tag="podcaster"

## HOW TO USE

See the example of how to use

### Extract audio from video

    <?php

    namespace Http\Controllers;
    
    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use KelvynCarbone\Podcaster\Podcaster;
    
    class PodcasterController extends Controller
    {
    
        public function extractAudio(Request $request){
            $url = $request->get("url");
            $podcaster = new Podcaster();
            $audio = $podcaster->extractAudio($url);
            return response()->json($audio);
        }
    }
    
    
## AWS CONFIG
In config/filesystem.php you need add some configs 
and check the keys match with .env AWS KEYS


    's3' => [
          'driver' => 's3',
          'key' => env('AWS_KEY'),
          'secret' => env('AWS_SECRET'),
          'region' => env('AWS_REGION'),
          'bucket' => env('AWS_BUCKET'),
          'http' => [
              'verify' => false
          ],
          'visibility' => 'public',
          'ACL' => 'public-read'
      ], 




## DONATE
To buy me a beer: https://link.pagar.me/lryonnIcuB
