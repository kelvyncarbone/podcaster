<?php

namespace KelvynCarbone\Podcaster;

use Illuminate\Support\Str;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\Format\Audio\Mp3;
use Illuminate\Support\Facades\Storage;

class Podcaster
{
    public function extractAudio($url, $name = null, $transcoding = false, $disks = ["s3"])
    {
        set_time_limit(-1);

        $name = isset($name) ? $name : Str::random(10) . ".mp3";
        $file = public_path($name);
        $audio_amazon_url = null;
        $audio_google_url = null;
        $transcoding = null;
        try {
            $ffmpeg = FFMpeg::create([
                'ffmpeg.binaries' => config('podcaster.ffmpeg_path'),
                'ffprobe.binaries' => config('podcaster.ffprobe_path'),
                'timeout' => 358658489,
                'read_timeout' => 358658489,
                'connect_timeout' => 358658489,
                'ffmpeg.threads' => 70,
            ]);

            $video = $ffmpeg->open($url);
            $format = new Mp3();
            $video->save($format, $file);

            foreach ($disks AS $disk) {
                switch ($disk) {
                    case "s3":
                        $audio_amazon_url = $this->saveAmazonS3($file, $name);
                    break;
                }
            }

            $ffprobe = FFProbe::create();
            $duration = $ffprobe->format($file)->get('duration');

            if($transcoding){
                $transcoding = $this->audioTranscoding($transcoding);
            }

            unlink($file);

            return [
                "title" => $name,
                "media_amazon_url" => $audio_amazon_url,
                "media_google_url" => $audio_google_url,
                "transcription_url" => $transcoding,
                "type" => "audio/mpeg",
                "time_length" => $duration
            ];
        }catch (\Exception $e) {
            try{
                unlink($file);
            }catch (\Exception $ex){}

            throw new \Exception($e);
        }
    }

    private function saveAmazonS3($file, $name)
    {
        $file = Storage::disk('s3')->put( config('podcaster.audio_directory')."/". $name, \File::get($file), 'public');
        return Storage::disk('s3')->url(config('podcaster.audio_directory')."/".$name);
    }

    public function generateRSSFromMedias(array $headerOptions, $medias)
    {
        PodcastFeed::setHeader([
            'title' => isset($headerOptions["title"]) ? $headerOptions["title"] : config("podcaster.title"),
            'subtitle' => isset($headerOptions["subtitle"]) ? $headerOptions["subtitle"] : config("podcaster.subtitle"),
            'description' => isset($headerOptions["description"]) ? $headerOptions["description"] : config("podcaster.description"),
            'link' => isset($headerOptions["link"]) ? $headerOptions["link"] : config("podcaster.link"),
            'image' => isset($headerOptions["cover"]) ? $headerOptions["cover"] : config("podcaster.cover"),
            'author' => isset($headerOptions["author"]) ? $headerOptions["author"] : config("podcaster.author"),
            'email' => isset($headerOptions["mail"]) ? $headerOptions["mail"] : config("podcaster.mail"),
            'category' => isset($headerOptions["category"]) ? $headerOptions["category"] : config("podcaster.category"),
            'language' => 'pt-br',
            'copyright' => isset($headerOptions["copyright"]) ? $headerOptions["copyright"] : 'Podcaster',
        ]);

        foreach ($medias AS $m) {
            PodcastFeed::addMedia([
                'title' => $m->title,
                'description' => $m->description,
                'guid' => $m->order,
                'publish_at' => $m->published_at,
                'url' => $m->media_amazon_url,
                'type' => $m->type,
                'duration' => $m->time_length,
                'image' => $m->cover
            ]);
        }

        return PodcastFeed::toString();
    }

    public function audioTranscoding($url)
    {

    }
}
