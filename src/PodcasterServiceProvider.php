<?php
namespace KelvynCarbone\Podcaster;

use Illuminate\Support\ServiceProvider as SupportServiceProvider;
use KelvynCarbone\Podcaster\Podcaster;

class PodcasterServiceProvider extends SupportServiceProvider{
    public function boot(){
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->mergeConfigFrom(__DIR__ . '/config/podcaster.php','podcaster');
        $this->publishes([
            __DIR__ . '/config/podcaster.php' => config_path('podcaster.php')
        ],'podcaster');
    }

    public function register(){
        $this->app->singleton(Podcaster::class,function($app){
            return new Podcaster();
        });

        $this->app->alias(Podcaster::class,'Podcaster');
    }

    public function provides(){
        return ['Podcaster'];
    }
}
