<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'client_secret' => env("PODCASTER_SECRET_KEY",null),
    'ffmpeg_path' => env('FFMPEG_PATH', '/usr/bin/ffmpeg'),
    'ffprobe_path' => env('FFPROBE_PATH', '/usr/bin/ffprobe'),
    'audio_directory' => 'audios',
    'aws_bucket' => env('AWS_BUCKET'),

    'title' => 'Podcaster',
    'subtitle' => 'Listening and change your life',
    'description' => 'Your description',
    'link' => 'http://yourlink.com',
    'cover' => '',
    'author' => 'Kelvyn Carbone',
    'mail' => 'kelvyn.carbone@gmail.com',
    'category' => 'Education'

];

